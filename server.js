import express from 'express';
import graphqHTTP from 'express-graphql';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import conSqlite from 'connect-sqlite3';
import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';
import uuid from 'uuid';
import db from './db';
import authograph from './auth/authograph';
import schema from './graphql/schema';

const app = express();
const SQliteStore = conSqlite(session);

const APP_PORT = process.env.PORT||"8080";
const APP_INTERFACE = process.env.INTERFACE||"127.0.0.1";

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  db.models.User.find({
    where: {
      id: id
    },
    include: {
      model: db.models.Role
    }
  })
  .then((user) => {
    if (!user)
      return done("Invalid Credentials");

    done(null, user);
  });
});

passport.use(new LocalStrategy({
  usernameField: "username",
  passwordField: "password",
  passReqToCallback: true
},
  (req, username, password, done) => {
    db.models.User.find({
      where: {
        email: username,
        password: password
      },
      include: {
        model: db.models.Role
      }
    })
    .then((user) => {
      if(!user)
        return done("Not Found", false);
      return done(null, user)
    })
  })
);
const LocalLogin = (req,res,next) => {
  passport.authenticate('local', (err,user,info) => {
    if(err || !user)
      return res.status(401).send("Invalid Credentials");
    req.login(user, {session:true},() => {
      next();
    })
  })(req,res)
};

const urlEncoded = bodyParser.urlencoded({extended:true});
app.use(bodyParser.json());
app.use(urlEncoded);
app.use(cookieParser());
app.use(session({
  store: new SQliteStore({
    db: "./lite",
    table: "sessions"
  }),
  genid: (req) => {
    return uuid.v1();
  },
  resave: false,
  saveUninitialized: false,
  secret: "secret"
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/graphql', [authograph.middleware(schema), (req,res) => {
  return graphqlHTTP(() => {
    return {
      schema: req.schema,
      graphiql: true,
    };
  })(req,res);
}]);

app.get('/test', (req, res) => {
  res.send("test");
})
app.get('/login', (req, res) => {
  res.send(`<form action="/login" method="post">
    <div>
        <label>Username:</label>
        <input type="text" name="username"/>
    </div>
    <div>
        <label>Password:</label>
        <input type="password" name="password"/>
    </div>
    <div>
        <input type="submit" value="Log In"/>
    </div>
</form>`)
});

app.post('/login', LocalLogin, (req,res,next) => {
  res.send(`Logged in as ${req.user.firstName} ${req.user.lastName}`);
});

app.get('/logout', (req,res,next) => {
  if(req.user) {
    console.log(`Logging out ${req.user.id}`);
    req.logout();
  }
  res.redirect('/login');
})

app.listen(APP_PORT, () => {
  console.log(`App is listening on ${APP_PORT}`)
})

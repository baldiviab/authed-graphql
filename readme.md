# Test setup
To install use ```git clone https://bitbucket.org/baldiviab/authed-graphql```
Run ```npm i``` to install required dependencies.

Set environment variables as needed:
```
PORT: <int> Interface port which server will listen on.
SYNCDB: <1> When set to 1 database will be synced
TESTDATA: <1> When set to 1 test data will be created
   Test account: login: asdf@asdf.com, password: asdf
```
Windows example:
```
set SYNCDB=1
set TESTDATA=1
```
Linux example:
```
export SYNCDB=1
export TESTDATA=1
```

## Running
To start server
```
npm start
```

## Usage
/graphql - GraphiQL inteface

/login - Login page

/logout - Force logout when navigating to

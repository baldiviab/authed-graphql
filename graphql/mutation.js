import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString
} from 'graphql';
import User from './user';
import Permission from './permission';
import _ from 'lodash';
import db from '../db';

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Mutation operation',
  fields() {
    return {
      addUser: {
        type: User,
        args: {
          firstName: {
            type: new GraphQLNonNull(GraphQLString)
          },
          lastName: {
            type: new GraphQLNonNull(GraphQLString)
          },
          password: {
            type: new GraphQLNonNull(GraphQLString)
          },
          email: {
            type: new GraphQLNonNull(GraphQLString)
          }
        },
        resolve(_, args, context) {
          return db.models.User.create({
            firstName: args.firstName,
            lastName: args.lastName,
            password: args.password,
            email: args.email.toLowerCase()
          })
        }
      },
      addPermission: {
        type: Permission,
        args: {
          type: {
            type: new GraphQLNonNull(GraphQLString)
          },
          field: {
            type: new GraphQLNonNull(GraphQLString)
          }
        },
        resolve(_, args, context) {
          return db.models.Permission.create({
            type: args.type,
            field: args.field
          });
        }
      },
      addRolePermission: {
        type: Permission,
        args: {
          type: {
            type: new GraphQLNonNull(GraphQLString)
          },
          field: {
            type: new GraphQLNonNull(GraphQLString)
          },
          arg: {
            type: GraphQLString
          },
          role: {
            type: GraphQLString
          }

        },
        resolve(_, args, context) {
          return db.models.Permission.findOrCreate({
            where: {
              type: args.type,
              field: args.field,
              arg: args.arg
            }
          })
          .then(([p]) => {
            if(!p || !args.role)
              return p;
            return db.models.Role.findOrCreate({
              where: {
                type: args.role
              }
            })
            .then(([role]) => {
              if(!role)
                return;
              return role.addPermission(p)
            })
            .then(() => {
              return p;
            });
          });
        }
      }
    }
  }
});

export default Mutation;

import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString
} from 'graphql';
import Role from './role';

const Permission = new GraphQLObjectType({
  name: 'Permission',
  description: 'This represents a Permission',
  fields() {
    return {
      id: {
        type: GraphQLInt,
        resolve(perm) {
          return perm.id;
        }
      },
      type: {
        type: GraphQLString,
        resolve(perm) {
          return perm.type;
        }
      },
      field: {
        type: GraphQLString,
        resolve(perm) {
          return perm.field;
        }
      },
      arg: {
        type: GraphQLString,
        resolve(perm) {
          return perm.arg
        }
      },
      roles: {
        type: new GraphQLList(Role),
        resolve(perm) {
          if(perm);
          return perm.getRoles();
        }
      }
    };
  }
});

export default Permission;

import {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLString
} from 'graphql';
import User from './user';
import _ from 'lodash';

const Post = new GraphQLObjectType({
  name: 'Post',
  description: 'This represents a Post',
  fields() {
    return {
      id: {
        type: GraphQLInt,
        resolve(post) {
          return post.id;
        }
      },
      title: {
        type: GraphQLString,
        resolve(post) {
          return post.title;
        }
      },
      content: {
        type: GraphQLString,
        resolve(post) {
          return post.content;
        }
      },
      user: {
        type: User,
        resolve(post) {
          return post.getUser();
        }
      }
    }
  }
});

export default Post;

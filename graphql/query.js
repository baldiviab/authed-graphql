import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLInt,
  GraphQLString
} from 'graphql';
import User from './user';
import Post from './post';
import Permission from './permission';
import Role from './role';
import _ from 'lodash';
import db from '../db';

const Query = new GraphQLObjectType({
  name: 'Query',
  description: 'This is a root query',
  fields() {
    return {
      users: {
        type: new GraphQLList(User),
        args: {
          id: {
            type: GraphQLInt
          },
          email: {
            type: GraphQLString
          }
        },
        resolve(root, args, context) {
          return db.models.User.findAll({where:args});
        }
      },
      posts: {
        type: new GraphQLList(Post),
        args: {
          id: {
            type: GraphQLInt
          },
          email: {
            type: GraphQLString
          }
        },
        resolve(root, args, context) {
          return db.models.Post.findAll({where:args});
        }
      },
      permissions: {
        type: new GraphQLList(Permission),
        args: {
          test: {
            type: GraphQLString
          }
        },
        resolve(root, args, context) {
          if(args.test)
            return db.query(`select * from Permissions`)
            .then((results) => {
              // Format results
              console.log(results);
            })
          return db.models.Permission.findAll({where:args});
        }
      },
      roles: {
        type: new GraphQLList(Role),
        resolve(root, args, context) {
          return db.models.Role.findAll({where: args});
        }
      },
      authed: {
        type: GraphQLString,
        resolve() {
          return "Authed Test"
        }
      }
    }
  }
});

export default Query;

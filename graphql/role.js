import {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLString
} from 'graphql';

const Role = new GraphQLObjectType({
  name: 'Role',
  description: 'This represents a Role',
  fields() {
    return {
      id: {
        type: GraphQLInt,
        resolve(role) {
          return role.id;
        }
      },
      type: {
        type: GraphQLString,
        resolve(role) {
          return role.type;
        }
      }
    };
  }
});

export default Role;

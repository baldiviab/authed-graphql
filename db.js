import Sequelize from 'sequelize';
import _ from 'lodash';
import faker from 'faker';

const sequelize = new Sequelize(
  "test",
  "asdf",
  "asdf",
  {
    dialect: "sqlite",
    host: "localhost",
    logging: false,
    storage: "./lite.db"
  }
);

const Version = sequelize.define('Version', {
  type: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  version: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0
  }
})

const Role = sequelize.define('Role', {
  type: {
    type: Sequelize.STRING,
    allowNull: false
  }
});

const User = sequelize.define('User', {
  firstName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      isEmail: true
    }
  }
})

const Post = sequelize.define('Post', {
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  content: {
    type: Sequelize.STRING,
    allowNull: false
  }
})

const Permission = sequelize.define('Permission', {
  type: {
    type: Sequelize.STRING,
    unique: "composite",
    allowNull: false
  },
  field: {
    type: Sequelize.STRING,
    unique: "composite",
    allowNull: false
  },
  arg: {
    type: Sequelize.STRING,
    unique: "composite"
  }
})


const UserRoles = sequelize.define('UserRoles', {});
const PermissionRoles = sequelize.define('PermissionRoles', {});

User.hasMany(Post);
Post.belongsTo(User);

// Authorization
User.belongsToMany(Role, {through: UserRoles});
Role.belongsToMany(User, {through: UserRoles});
Permission.belongsToMany(Role, {through: PermissionRoles});
Role.belongsToMany(Permission, {through: PermissionRoles});

var sync = process.env.SYNCDB === "1";
var testData = process.env.TESTDATA === "1";

sequelize.sync({force: sync})
.then(() => {
  var rootUser;
  if(testData)
    User.create({
      firstName: "Ben",
      lastName: "Baldivia",
      password: "asdf",
      email: "asdf@asdf.com"
    })
    .then((ru) => {
      rootUser = ru;
      return Role.create({type: "admin"})
    })
    .then((admin) => {
      rootUser.addRole(admin);
    })
    .then((su) => {
      _.times(10, () => {
          return User.create({
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            password: faker.internet.password(),
            email: faker.internet.email()
          })
          .then((user) => {
            return user.createPost({
              title: `Post by ${user.firstName}`,
              content: faker.lorem.words(20)
            })
          })
      });
    });
});

export default sequelize;

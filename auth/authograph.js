import graphqlHTTP from 'express-graphql';
import * as GraphQLTypes from 'graphql/type';
import Authograph from 'authograph';
import * as Bounds from './bounds';
import Types from '../graphql/_types';
import Query from '../graphql/query';
import Mutation from '../graphql/mutation';
import db from '../db';

const instance = new Authograph({
  getRoles(req) {
    var user = req.user||{};
    if(!(user.Roles instanceof Array))
      return Promise.resolve([]);
    return Promise.resolve(user.Roles.map(o => o.type));
  },
  buildLegacyPSet(roleIds) {

  },
  buildPSet(roleIds) {
      return {
        Query: {
          users: {
            id: {
              admin: {
                min: 3,
                max: 5
              }
            }
          },
          posts: {
            _: {
              admin: {}
            }
          }
        },
        User: {
          test: {
            _: {
              admin: {}
            }},
          firstName: {
            _: {
              admin: {}
            }},
          posts: {
            _: {
              admin: {}
            }}
        },
        Post: {
          id: {
            _: {
              admin: {}
            }}
        }
      };
    return db.models.Permission.findAll({
      include: {
        model: db.models.Role,
        required: true,
        where: {
          type: {
            $in: roleIds
          }
        }
      }
    })
    .then((permissions) => {
      return permissions
      .reduce((r,p) => {
        p.Roles.forEach((role) => {
          let bounds = {}
          if(!r)
            r = {};
          if(!r[p.type])
            r[p.type] = {};
          if(!r[p.type][p.field])
            r[p.type][p.field] = {};
          if(p.arg) {
            if(!r[p.type][p.field][p.arg])
              r[p.type][p.field][p.arg] = {};
            if(bounds) {
              r[p.type][p.field][p.arg][role.type] = bounds;
            }
          }
        });
        return r;
      },{});
    });
  }
});

export default instance;

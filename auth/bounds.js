export const min = (arg, bound) => (root, args, context) => {
  if(!args[arg])
    return true;
  return arg[arg] > bound;
};

export const max = (arg, bound) => (root, args, context) => {
  if(!args[arg])
    return true;
  return arg[arg] < bound;
}

export const oneOf = (arg, bound) => (root, args, context) => {
  if(!args[arg])
    return true;
  if(!(bound instanceof Array))
    return args[arg] === bound;
  return bound.indexOf(args[arg]) !== -1;
}